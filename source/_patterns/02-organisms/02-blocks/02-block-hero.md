---
title: Secondary Hero
---

The secondary hero is a large block that contains a large full-bleed image, title, and call to action. May also contain descriptive text.
