---
title: Primary Hero
---

The secondary hero is a large block that is usually found at the top of the page, contains a large full-bleed image, title, and call to action.
