var grid = document.querySelector('.section-masonry-grid');
var msnry;

if(grid) {
	imagesLoaded( grid, function() {
	  // init Isotope after all images have loaded
	  msnry = new Masonry( grid, {
	    itemSelector: '.single-masonry-grid',
	    gutter: 16
	  });
	});
}
