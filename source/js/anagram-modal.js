/**
* Setup modals
* @see https://robinparisi.github.io/tingle/
*/

// instanciate new modal: media archive
var modal = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['anagram-modal-card'],
});

var tingle_button = document.querySelectorAll('.open-modal .anagram-single-card');

for (i = 0; i < tingle_button.length; ++i) {
  	tingle_button[i].addEventListener('click', function(){
  		var modalId = this.getAttribute('data-modal');
  		var modalContent = document.querySelector('#'+ modalId);
  		modal.setContent(modalContent.innerHTML);
	  	modal.open();
	});
}


// instanciate new modal: cta form
var modalForm = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['anagram-modal-form'],
});

var tingle_button_form = document.querySelectorAll('.open-form .btn');

for (i = 0; i < tingle_button_form.length; ++i) {
    tingle_button_form[i].addEventListener('click', function(){
        var modalIdForm = this.parentNode.getAttribute('data-form-modal');
        var modalContentForm = document.querySelector('#'+ modalIdForm);
        modalForm.setContent(modalContentForm.innerHTML);
        modalForm.open();
    });
}

// instanciate new modal: gallery
var modalGallery = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['anagram-modal-gallery'],
});

var tingle_button_gallery = document.querySelectorAll('.open-modal .single-masonry-grid');

for (i = 0; i < tingle_button_gallery.length; ++i) {
    tingle_button_gallery[i].addEventListener('click', function(){
        var modalIdGallery = this.getAttribute('data-modal');
        var modalContentGallery = document.querySelector('#'+ modalIdGallery);
        modalGallery.setContent(modalContentGallery.innerHTML);
        modalGallery.open();
    });
}