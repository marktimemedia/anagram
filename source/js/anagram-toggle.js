/* Name: Toggle Searchbar
* Author: Marktime Media
* Author URI: http://marktimemedia.com
* Version: 0.1
* License: GPLv2
*  
* This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License version 2,
*  as published by the Free Software Foundation.
* 
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*  GNU General Public License for more details.
*  
*  The license for this software can likely be found here:
*  http://www.gnu.org/licenses/gpl-2.0.html
*/


// Add event listeners to buttons
var linkToggleBtn = document.getElementById('link-toggle');

if(linkToggleBtn) {
    if (linkToggleBtn.attachEvent) {
        linkToggleBtn.attachEvent('onclick', openCloseToggle);
    } else {
        linkToggleBtn.addEventListener('click', openCloseToggle);
    }
}


// add and remove classes for search toggle
function openCloseToggle() {
    var wrapperEl = document.getElementById('toggle-area');
    var wrapperElClasses = wrapperEl.className;
    var linkEl = document.getElementById('link-toggle');
    var linkElClasses = linkEl.className;

    // Add or remove the search-open class to the wrapper
    if (wrapperElClasses.indexOf('hidden') === -1) {
        wrapperEl.className = wrapperElClasses + ' hidden';
    } else {
        wrapperEl.className = wrapperElClasses.replace(' hidden', '');
    }

    // Add or remove the read-less class to the link
    if (linkElClasses.indexOf('read-less') === -1) {
        linkEl.className = linkElClasses + ' read-less';
    } else {
        linkEl.className = linkElClasses.replace(' read-less', '');
    }
}