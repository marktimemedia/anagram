var container = document.querySelector('.insta-list');

if( container ) {

	// Siema
	new Siema({
		selector: container,
		duration: 200,
        easing: 'ease-out',
        startIndex: 0,
        draggable: true,
		loop: true,
		perPage: {
			180: 1.5,
			640: 2.5,
			960: 3.5,
			1180: 4.5
		},
		onInit: init,
	});

	// prevent link from clicking while scrolling
	//https://stackoverflow.com/questions/19172084/plain-javascript-version-of-e-preventdefault
	function init() {
	    var links = document.querySelectorAll( '.insta-list a' );
	    var mousedownTime;
	    var mousedownNew;

		for( var i = 0; i < links.length; i++ ) {
			links[i].addEventListener( 'mousedown', startHoldTimer, false );
			links[i].addEventListener( 'click', checkHolderTimer, false );
		}
	}

	function startHoldTimer() {
		mousedownTime = new Date();
	}

	function checkHolderTimer(e) {
		mousedownNew = new Date();

		if( ( mousedownNew - mousedownTime ) > 100 ) {
			e.preventDefault();
			//console.log(mousedownNew - mousedownTime + ' should prevent default');
		}
	}
}

