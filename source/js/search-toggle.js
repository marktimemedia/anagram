/* Name: Toggle Searchbar
* Author: Marktime Media
* Author URI: http://marktimemedia.com
* Version: 0.1
* License: GPLv2
*  
* This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License version 2,
*  as published by the Free Software Foundation.
* 
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*  GNU General Public License for more details.
*  
*  The license for this software can likely be found here:
*  http://www.gnu.org/licenses/gpl-2.0.html
*/


// Add event listeners to buttons
var searchToggleBtn = document.getElementById('search-toggle-btn');

if (searchToggleBtn.attachEvent) {
    searchToggleBtn.attachEvent('onclick', openCloseSearch);
} else {
    searchToggleBtn.addEventListener('click', openCloseSearch);
}

// add and remove classes for search toggle
function openCloseSearch() {
    var wrapperEl = document.querySelectorAll('div.wrapper')[0];
    var searchEl = document.querySelectorAll('div.search-form')[0];
    var focusEl = document.getElementById('search');
    var wrapperElClasses = wrapperEl.className;
    var searchElClasses = searchEl.className;

    // Add or remove the search-open class to the wrapper
    if (wrapperElClasses.indexOf('search-open') === -1) {
        wrapperEl.className = wrapperElClasses + ' search-open';
    } else {
        wrapperEl.className = wrapperElClasses.replace(' search-open', '');
    }

    // Add or remove the search-expanded class to the search
    if (searchElClasses.indexOf('search-expanded') === -1) {
        searchEl.className = searchElClasses + ' search-expanded';
        focusEl.focus();
    } else {
        searchEl.className = searchElClasses.replace(' search-expanded', '');
    }
}